local cmp = require('cmp')
local nvim_lsp = require('lspconfig')
require('rust-tools').setup({})

-- LSP Keybindings
local opts = {noremap = true}
local on_attach = function(client, bufnr)
  local opts = {buffer=0}
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opt)
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
  vim.keymap.set('n', '<leader>l', vim.diagnostic.goto_next, opts)
  vim.keymap.set('n', '<leader>h', vim.diagnostic.goto_next, opts)
  vim.keymap.set('n', '<F3>', vim.lsp.buf.code_action, opts)
end

-- LUA Snip
cmp.setup({
	snippet = {
		expand = function(args)
			require('luasnip').lsp_expand(args.body)
		end,
	},
	mapping = {
		['<CR>'] = cmp.mapping.confirm({ select = true }),
		['<C-p>'] = cmp.mapping.select_prev_item(),
		['<C-n>'] = cmp.mapping.select_next_item(),
	},
	sources = cmp.config.sources({
		{ name = 'nvim_lsp' },
	})
})

local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())

-- Rust
nvim_lsp['rust_analyzer'].setup {
        cmd = { "rust-analyzer" },
        settings = {
                ["rust-analyzer"] = {
                        diagnostics = {
                                enable = true,
                                disabled = {"unresolved-proc-macro"},
                                enableExperimental = true,
                        },
                }
        },
        on_attach = on_attach,
        capabilities = capabilities
}

-- Go
nvim_lsp['gopls'].setup {
        on_attach = on_attach,
        capabilities = capabilities
}

-- C/C++
nvim_lsp['clangd'].setup {
        on_attach = on_attach,
        capabilities = capabilities
}

-- Python
nvim_lsp['pyright'].setup {
        on_attach = on_attach,
        capabilities = capabilities
}

-- Typescript
nvim_lsp['tsserver'].setup {
        on_attach = on_attach,
        capabilities = capabilities
}

