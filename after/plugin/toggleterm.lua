require("toggleterm").setup{
        size = 100,
        open_mapping = [[<c-\>]],
        direction = "vertical",
        hide_numbers = true,
        close_on_exit = true,
}
