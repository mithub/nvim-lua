local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

return require("packer").startup(function()
	use("wbthomason/packer.nvim")
	use 'neovim/nvim-lspconfig'
	use 'simrat39/rust-tools.nvim'
	use 'nvim-lua/plenary.nvim'
	use 'mfussenegger/nvim-dap'
	use 'L3MON4D3/LuaSnip'
	use 'saadparwaiz1/cmp_luasnip'
	use 'hrsh7th/cmp-nvim-lsp'
	use 'hrsh7th/nvim-cmp'
	use 'windwp/nvim-autopairs'
	use 'akinsho/toggleterm.nvim'
	use { 'nvim-telescope/telescope.nvim', requires = {'nvim-lua/plenary.nvim'} }
	use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
	use 'nvim-treesitter/nvim-treesitter'
	use 'folke/tokyonight.nvim'
	use 'nvim-treesitter/nvim-treesitter-context'

	-- Ensure Packer is installed
	if packer_bootstrap then
		require('packer').sync()
	end
end)
